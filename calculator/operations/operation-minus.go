package operations

import (
	"fmt"
	"go-level1/calculator/calculator/shared"
)

const Minus shared.OperationType = "-"

type OperationMinus struct {
	a, b *float64
}

var _ shared.Operation = &OperationMinus{}

func NewMinus() *OperationMinus {
	return &OperationMinus{}
}

func (o *OperationMinus) Init(operands ...float64) shared.Operation {
	if len(operands) >= 2 {
		o.a, o.b = &operands[0], &operands[1]
	}

	return o
}

func (o *OperationMinus) Calc() (float64, error) {
	if o.a == nil || o.b == nil {
		return 0, fmt.Errorf("ошибка инициализации")
	}
	return *o.a - *o.b, nil
}

func (o *OperationMinus) Match(operationType shared.OperationType) bool {
	return operationType == Minus
}
