package operations

import (
	"fmt"
	"go-level1/calculator/calculator/shared"
)

const Multiply shared.OperationType = "*"

type OperationMultiply struct {
	a, b *float64
}

var _ shared.Operation = &OperationMultiply{}

func NewMultiply() *OperationMultiply {
	return &OperationMultiply{}
}

func (o *OperationMultiply) Init(operands ...float64) shared.Operation {
	if len(operands) >= 2 {
		o.a, o.b = &operands[0], &operands[1]
	}

	return o
}

func (o *OperationMultiply) Calc() (float64, error) {
	if o.a == nil || o.b == nil {
		return 0, fmt.Errorf("ошибка инициализации")
	}
	return *o.a * *o.b, nil
}

func (o *OperationMultiply) Match(operationType shared.OperationType) bool {
	return operationType == Multiply
}
