package operations

import (
	"fmt"
	"go-level1/calculator/calculator/shared"
)

const Divide shared.OperationType = "/"

type OperationDivide struct {
	a, b *float64
}

var _ shared.Operation = &OperationDivide{}

func NewDivide() *OperationDivide {
	return &OperationDivide{}
}

func (o *OperationDivide) Init(operands ...float64) shared.Operation {
	if len(operands) >= 2 {
		o.a, o.b = &operands[0], &operands[1]
	}

	return o
}

func (o *OperationDivide) Calc() (float64, error) {
	if o.a == nil || o.b == nil {
		return 0, fmt.Errorf("ошибка инициализации")
	}

	if *o.b == 0 {
		return 0, fmt.Errorf("деление на ноль запрещено")
	}
	return *o.a / *o.b, nil
}

func (o *OperationDivide) Match(operationType shared.OperationType) bool {
	return operationType == Divide
}
