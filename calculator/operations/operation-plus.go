package operations

import (
	"fmt"
	"go-level1/calculator/calculator/shared"
)

const Plus shared.OperationType = "+"

type OperationPlus struct {
	a, b *float64
}

var _ shared.Operation = &OperationPlus{}

func NewPlus() *OperationPlus {
	return &OperationPlus{}
}

func (o *OperationPlus) Init(operands ...float64) shared.Operation {
	if len(operands) >= 2 {
		o.a, o.b = &operands[0], &operands[1]
	}

	return o
}

func (o *OperationPlus) Calc() (float64, error) {
	if o.a == nil || o.b == nil {
		return 0, fmt.Errorf("ошибка инициализации")
	}
	return *o.a + *o.b, nil
}

func (o *OperationPlus) Match(operationType shared.OperationType) bool {
	return operationType == Plus
}
