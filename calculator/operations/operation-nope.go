package operations

import (
	"fmt"
	"go-level1/calculator/calculator/shared"
)

const Nope shared.OperationType = ""

type OperationNope struct{}

var _ shared.Operation = &OperationNope{}

func NewNope() *OperationNope {
	return &OperationNope{}
}

func (o *OperationNope) Init(operands ...float64) shared.Operation {
	return o
}

func (o *OperationNope) Calc() (float64, error) {
	return 0, fmt.Errorf("действие не указано")
}

func (o *OperationNope) Match(operationType shared.OperationType) bool {
	return operationType == Nope
}
