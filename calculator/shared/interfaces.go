package shared

type Operation interface {
	Init(operands ...float64) Operation

	Calc() (float64, error)
	Match(operationType OperationType) bool
}
