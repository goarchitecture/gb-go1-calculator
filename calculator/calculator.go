package calculator

import (
	"fmt"
	"go-level1/calculator/calculator/operations"
	"go-level1/calculator/calculator/shared"
	sqrtPlugin "go-level1/calculator/sqrt-plugin"
)

type Calculator struct {
	operations []shared.Operation

	operationType      shared.OperationType
	operand1, operand2 float64
}

func NewStandardCalculator() *Calculator {
	fmt.Println()
	return &Calculator{
		operations: []shared.Operation{
			operations.NewNope(),

			operations.NewPlus(),
			operations.NewMinus(),
			operations.NewDivide(),
			operations.NewMultiply(),
		},
	}
}

func NewAdvancedCalculator() *Calculator {
	return NewStandardCalculator().AppendOperation(sqrtPlugin.NewSqrt())
}

func (c *Calculator) AppendOperation(operations ...shared.Operation) *Calculator {
	c.operations = append(c.operations, operations...)
	return c
}

func (c *Calculator) SetOperationType(opType shared.OperationType) *Calculator {
	c.operationType = opType
	return c
}

func (c *Calculator) SetOperands(a, b float64) *Calculator {
	c.operand1, c.operand2 = a, b
	return c
}

func (c *Calculator) Calc() (result float64, err error) {
	for _, op := range c.operations {
		if op.Match(c.operationType) {
			return op.Init(c.operand1, c.operand2).Calc()
		}
	}

	return 0, fmt.Errorf("не поддерживаемая операция")
}
