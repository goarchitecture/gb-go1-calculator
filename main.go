package main

import (
	"fmt"
	"go-level1/calculator/calculator"
	"os"
)

func main() {
	a, b, op, err := calculator.ReadInput()
	if err != nil {
		fmt.Printf("ошибка считывания: %s\n", err)
		os.Exit(1)
	}

	calc := calculator.NewAdvancedCalculator()

	result, err := calc.SetOperationType(op).SetOperands(a, b).Calc()
	if err != nil {
		fmt.Printf("ошибка вычисления: %s\n", err)
		os.Exit(1)
	}

	fmt.Printf("Результат = %5.2f\n", result)
}
