package sqrtPlugin

import (
	"fmt"
	"go-level1/calculator/calculator/shared"
	"math"
)

const Sqrt shared.OperationType = "S"

type OperationSqrt struct {
	a *float64
}

var _ shared.Operation = &OperationSqrt{}

func NewSqrt() *OperationSqrt {
	return &OperationSqrt{}
}

func (o *OperationSqrt) Init(operands ...float64) shared.Operation {
	if len(operands) >= 1 {
		o.a = &operands[0]
	}

	return o
}

func (o *OperationSqrt) Calc() (float64, error) {
	if o.a == nil {
		return 0, fmt.Errorf("ошибка инициализации")
	}
	return math.Sqrt(*o.a), nil
}

func (o *OperationSqrt) Match(operationType shared.OperationType) bool {
	return operationType == Sqrt
}
